import { Directive, ElementRef, HostListener } from '@angular/core';
import { CursorService } from '../services/cursor.service';

@Directive({
  selector: '[clickable]'
})
export class CursorDirective {

  constructor(private el: ElementRef, private cursorService: CursorService) {}

  @HostListener('mouseover') onMouseEnter() {
    this.cursorService.onEnterHover();
    console.log("enter");

  }

  @HostListener('mouseout') onMouseLeave() {
    this.cursorService.onLeaveHover();
    console.log("leave");
  }

}
