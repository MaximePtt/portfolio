import { AfterViewInit, Component, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { CursorService } from './services/cursor.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'portfolio';

  constructor(private renderer: Renderer2, private el: ElementRef, public cursorService: CursorService) {}

  ngAfterViewInit(): void {}

  @HostListener('document:mousemove', ['$event'])
  handleMouseMove(event: MouseEvent) {
    const curseur = document.getElementById('cursor');
    curseur!.style.left = event.pageX + 'px';
    curseur!.style.top = event.pageY + 'px';
  }

  @HostListener('document:mousedown')
  handleMouseDown() {
    const curseur = document.getElementById('cursor');
    this.cursorService.onEnterClick()
  }

  @HostListener('document:mouseup')
  handleMouseUp() {
    const curseur = document.getElementById('cursor');
    this.cursorService.onLeaveClick()
  }
}
