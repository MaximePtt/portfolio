import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CursorService {

  hovered = false;
  clicked = false;

  constructor() { }


  onEnterHover(){
    this.hovered = true
  }

  onLeaveHover(){
    this.hovered = false
  }

  onEnterClick(){
    this.clicked = true
  }

  onLeaveClick(){
    this.clicked = false
  }
}
