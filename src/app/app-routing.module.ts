import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeaComponent } from './components/sea-component/sea.component';

const routes: Routes = [
  { path: 'sea', component: SeaComponent },
  { path: '**', redirectTo: "sea" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
