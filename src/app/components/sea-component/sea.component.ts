import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-sea',
  templateUrl: './sea.component.html',
  styleUrls: ['./sea.component.scss']
})
export class SeaComponent implements AfterViewInit{
  @ViewChild("bubble") bubbleRef!: ElementRef;
  @ViewChild("bubbleContainer") bubbleContainerRef!: ElementRef;

  isProjectSwitchingBack = false
  isProjectSwitchingNext = false
  isSpeeding = false;
  isUnderWater = false;

  currentProjectIndex = 0;
  projects = [
    {
      name: "Premier projet",
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor laborum rem a nostrum, nam minima magni placeat illo nisi id delectus quos laboriosam dolorum ipsum ab fuga inventore, molestias assumenda."
    },
    {
      name: "Second projet",
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor laborum rem a nostrum, nam minima magni placeat illo nisi id delectus quos laboriosam dolorum ipsum ab fuga inventore, molestias assumenda."
    },
    {
      name: "Troisième projet",
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor laborum rem a nostrum, nam minima magni placeat illo nisi id delectus quos laboriosam dolorum ipsum ab fuga inventore, molestias assumenda."
    }
  ]

  ngAfterViewInit(): void {
    this.bubbleRef.nativeElement.addEventListener('animationiteration', () => {
      this.bubbleSetRandomPosition()
    });
  }

  bubbleSetRandomPosition() {
    let containerWidth = this.bubbleContainerRef!.nativeElement.clientWidth;
    let bubbleWidth = this.bubbleRef!.nativeElement.clientWidth;
    console.log(containerWidth, bubbleWidth, Math.floor(Math.random() * (containerWidth - bubbleWidth + 1)));

    let randomLeft = Math.floor(Math.random() * (containerWidth - bubbleWidth + 1));
    this.bubbleRef!.nativeElement.style.left = `${randomLeft}px`;
  }

  goUnderWater(){
    this.isUnderWater = true
  }

  goOnWater(){
    this.isUnderWater = false
  }

  goToNextElement(){
    this.isProjectSwitchingNext = true
    setTimeout(() => {
      this.currentProjectIndex++
      setTimeout(() => {
        this.isProjectSwitchingNext = false
      }, 100);
    }, 100);

    this.isSpeeding = true
    setTimeout(() => {
      this.isSpeeding = false
    }, 2000);
  }

  goToBackElement(){
    this.isProjectSwitchingBack = true
    setTimeout(() => {
      this.currentProjectIndex--
      setTimeout(() => {
        this.isProjectSwitchingBack = false
      }, 100);
    }, 100);

    this.isSpeeding = true
    setTimeout(() => {
      this.isSpeeding = false
    }, 2000);
  }
}
